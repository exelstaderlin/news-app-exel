package com.exel.newsappexel

import android.annotation.SuppressLint
import org.junit.Assert
import org.junit.Test
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class UtilTest {

    @Test
    @SuppressLint("SimpleDateFormat")
    fun formatDate() { //covert string date ke format yg di inginkan
        for (zDate in MockData.MOCK_DATE) {
            val inputFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val outputFormat = SimpleDateFormat("MMM dd, yyyy hh:mm a")
            var date: Date? = null
            try {
                date = inputFormat.parse(zDate)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            outputFormat.timeZone = TimeZone.getTimeZone("Asia/Jakarta")
            Assert.assertEquals("Sat Aug 02 11:11:22 ICT 2008", date)
            Assert.assertEquals(true, date != null)
            Assert.assertEquals(false, date == null)
            Assert.assertEquals("Sat Aug 02 11:11:22 ICT 2008", date)
            Assert.assertEquals("Sat Aug 02 11:11:22 ICT 2008", date)
        }
    }

}