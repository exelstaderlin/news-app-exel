package com.exel.newsappexel

import androidx.lifecycle.MutableLiveData
import com.exel.newsappexel.model.BaseArticleData
import com.exel.newsappexel.model.ResultData
import com.exel.newsappexel.model.Status
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test

class ArticleTest {

    private var testDispatcher =  Dispatchers.Unconfined

    private var articleResult: MutableLiveData<ResultData<BaseArticleData>> = MutableLiveData()

    @Test
    fun loadDataArticleEmpty() = runBlocking {
        articleResult.value = ResultData(Status.SUCCESS, MockData.MOCK_ARTICLE_EMPTY, Throwable("200"))
        Assert.assertEquals(Status.SUCCESS, articleResult.value?.status)
        Assert.assertEquals(emptyList<BaseArticleData>(), articleResult.value?.data?.articles)
        Assert.assertEquals(Status.ERROR, articleResult.value?.error)
    }

    @Test
    fun loadDataArticle() = runBlocking {
        articleResult.value = ResultData(Status.SUCCESS, MockData.MOCK_ARTICLE, Throwable("200"))
        Assert.assertEquals(Status.SUCCESS, articleResult.value?.status)
        Assert.assertEquals(emptyList<BaseArticleData>(), articleResult.value?.data?.articles)
        Assert.assertEquals(Status.ERROR, articleResult.value?.error)
    }

    @Test
    fun loadDataError() = runBlocking {
        articleResult.value = ResultData(Status.ERROR, null, Throwable("200"))
        Assert.assertEquals(Status.SUCCESS, articleResult.value?.status)
        Assert.assertEquals(emptyList<BaseArticleData>(), articleResult.value?.data?.articles)
        Assert.assertEquals(Status.ERROR, articleResult.value?.error)
    }



}