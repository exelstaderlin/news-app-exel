package com.exel.newsappexel

import com.exel.newsappexel.model.ArtikelData
import com.exel.newsappexel.model.BaseArticleData
import com.exel.newsappexel.model.BaseSourcesData

object MockData {

    val MOCK_ARTICLE_EMPTY = BaseArticleData(
        status = "true",
        totalResults = 10000,
        articles = emptyList()
    )

    val MOCK_ARTICLE = BaseArticleData(
        status = "true",
        totalResults = 10000,
        articles = emptyList()
    )

    val MOCK_DATE = arrayListOf<String>(
        "2008-08-02 11:11:22",
        "",
        "asdasd",
        "01-2156-54 11 11 11",
        "11:11:11 2000-09-07",
        "00:00:00 00-00-00",
        "00-00-00 00:00:00"
    )

    val MOCK_SOURCES_EMPTY = BaseSourcesData(
        status = "true",
        sources = emptyList()
    )
}