package com.exel.newsappexel.api

/**
 * Created by Exel staderlin on 7/1/2017.
 */

const val URL = "https://newsapi.org/v2/"
const val TOKEN = "43c3666f47964e31a8db69be3ad611f4" //my_personal_github_token
const val VISIBLE_THRESHOLD = 5
const val NETWORK_PAGE_SIZE = 10
const val VISIBLE = true
const val GONE = false
