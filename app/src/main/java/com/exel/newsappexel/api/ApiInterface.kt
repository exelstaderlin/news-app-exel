package com.exel.newsappexel.api


import com.exel.newsappexel.model.BaseArticleData
import com.exel.newsappexel.model.BaseSourcesData
import com.exel.newsappexel.model.ResultData
import retrofit2.http.*

/**
 * Created by Exel staderlin on 7/12/2017.
 */
interface ApiInterface {

    @GET("everything")
    suspend fun searchNews(
        @Query("q") username: String,
        @Query("page") page: Int,
        @Query("pageSize") itemsPerPage: Int
    ): BaseArticleData

    @GET("sources")
    suspend fun getSources(
        @Query("category") category: String,
        @Query("language") language: String
    ): BaseSourcesData

    @GET("everything")
    suspend fun getArticles(
        @Query("sources") source: String,
        @Query("page") page: Int,
        @Query("pageSize") itemsPerPage: Int
    ): BaseArticleData

    @GET("top-headlines")
    suspend fun getTopHeadLines(
        @Query("country") country: String,
        @Query("category") category: String,
        @Query("page") page: Int,
        @Query("pageSize") itemsPerPage: Int
    ): BaseArticleData
}
