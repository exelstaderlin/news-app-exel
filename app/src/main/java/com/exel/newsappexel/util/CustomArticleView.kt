package com.exel.newsappexel.util

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.exel.newsappexel.databinding.CustomArticleViewBinding

class CustomArticleView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    var mBinding: CustomArticleViewBinding? = null

    init {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        mBinding = CustomArticleViewBinding.inflate(inflater, this, true)
    }

    var categoryVisibility: Boolean = false
        set(value) {
            field = value
            mBinding?.category?.visibility = if(categoryVisibility) View.VISIBLE else View.GONE
        }
}