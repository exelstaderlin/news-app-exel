package com.exel.newsappexel.util


import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.exel.newsappexel.R
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


    @SuppressLint("InflateParams")
    fun showAlertDialog(context: Activity, message: String, onValid: () -> Unit) {
        val dialog: AlertDialog
        val builder = AlertDialog.Builder(context)
        val inflater = context.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_alert, null)

        val messages = dialogView.findViewById<TextView>(R.id.message)
        val ok = dialogView.findViewById<TextView>(R.id.positive_button)

        builder.setView(dialogView)
        messages.text = message
        dialog = builder.create()
        dialog.show()
        dialog.setCancelable(false)
        ok.setOnClickListener {
            dialog.dismiss()
            onValid()
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun formatDate(zDate: String?): String? { //covert string date ke format yg di inginkan
        val inputFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val outputFormat = SimpleDateFormat("MMM dd, yyyy hh:mm a")
        var date: Date? = null
        try {
            date = inputFormat.parse(zDate)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        outputFormat.timeZone = TimeZone.getTimeZone("Asia/Jakarta")
        return outputFormat.format(date)
    }

    @SuppressLint("SimpleDateFormat")
    fun formatDateTimezone(inputTime: String?) : String {
        val inputTz = TimeZone.getTimeZone("GMT")
        val utcTz = TimeZone.getTimeZone("Asia/Jakarta")

        val inputSdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX")
        inputSdf.timeZone = inputTz

        val utcSdf = SimpleDateFormat("MMM dd, yyyy | hh:mm a")
        utcSdf.timeZone = utcTz
        // From time
        var fromDate: Date? = null
        try {
            if (inputTime == "" || inputTime == null)
                return  ""
            else
                fromDate = inputSdf.parse(inputTime)
        } catch (e: ParseException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }
        // Convert to UTC
        return utcSdf.format(fromDate)
    }



    fun checkInternetConnection(context: Context): Boolean {
        val localNetwork =
            (context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo
        return localNetwork != null
    }

