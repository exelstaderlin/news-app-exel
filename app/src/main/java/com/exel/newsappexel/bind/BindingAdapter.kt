package com.exel.newsappexel.bind

import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import com.exel.newsappexel.R
import com.exel.newsappexel.ui.main.WebViewActivity
import com.exel.newsappexel.util.formatDateTimezone
import com.squareup.picasso.Picasso

object BindingAdapters {

    @BindingAdapter("setImage")
    @JvmStatic
    fun setImage(view: ImageView, urlImage: String?) {
        when {
            urlImage.isNullOrEmpty() -> Picasso.get()
                .load(R.drawable.ic_image)
                .fit()
                .centerCrop()
                .placeholder(R.drawable.ic_image)
                .error(R.drawable.ic_image)
                .into(view)
            else ->  Picasso.get()
                .load(urlImage)
                .fit()
                .centerCrop()
                .placeholder(R.drawable.ic_image)
                .error(R.drawable.ic_image)
                .into(view)
        }
    }

    @BindingAdapter("setDate")
    @JvmStatic
    fun setDate(view: AppCompatTextView, date: String?) {
        view.text = formatDateTimezone(date)
    }

    @BindingAdapter("setIcon")
    @JvmStatic
    fun setIcon(view: AppCompatImageView, icon: Int) {
        view.setImageResource(icon)
    }


}
