package com.exel.newsappexel.viewmodel

import androidx.lifecycle.MutableLiveData
import com.exel.newsappexel.api.VISIBLE_THRESHOLD
import com.exel.newsappexel.model.ArtikelData
import com.exel.newsappexel.model.BaseArticleData
import com.exel.newsappexel.model.ResultData
import com.exel.newsappexel.repository.ArticleRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
class ArticleViewModel @Inject constructor(
    private var articleRepository: ArticleRepository
) : BaseViewModel() {

    private var page: Int = 1
    private var mSource: String = "1"

    val articleResult =
        MutableLiveData<ResultData<BaseArticleData>>() //Triggered LiveData when data is ready

    fun getArticleData(source: String) {
        page = 1
        mSource = source
        getArticles()
    }

    fun listScrolled(visibleItemCount: Int, firstVisibleItemPosition: Int, totalItemCount: Int) {
        if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
            && firstVisibleItemPosition >= 0
            && totalItemCount >= VISIBLE_THRESHOLD
        ) {
            if (!loadingRunning.value!!) { // avoid triggering multiple requests in the same time
                page++
                if (emptyLastItem.value == false) {
                    getArticles()
                    loadMoreLiveData.value = true
                } //not search when list is empty

            }
        }
    }

    private fun getArticles() {
        val flowData = articleRepository.getArticles(mSource, page)
        launchDataLoad(flowData,
            loading = { articleResult.value = ResultData.loading() },
            error = { err -> articleResult.value = ResultData.error(err) },
            success = { response ->
                articleResult.value = ResultData.success(response)
                emptyLastItem.value = response.articles?.isEmpty()
            }
        )
    }
}