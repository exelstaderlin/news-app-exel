package com.exel.newsappexel.viewmodel

import androidx.lifecycle.MutableLiveData
import com.exel.newsappexel.model.BaseSourcesData
import com.exel.newsappexel.model.ResultData
import com.exel.newsappexel.model.SourceData
import com.exel.newsappexel.repository.SourcesRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.lang.Exception
import javax.inject.Inject

@ExperimentalCoroutinesApi
class SourcesViewModel @Inject constructor(
    private var sourcesRepository: SourcesRepository
) : BaseViewModel() {
    val sourceResult =
        MutableLiveData<ResultData<BaseSourcesData>>() //Triggered LiveData when data is ready

    fun getAllData(category: String) {
        val flowData = sourcesRepository.getSources(category)
        launchDataLoad(flowData,
            loading = { sourceResult.value = ResultData.loading() },
            error = { err -> sourceResult.value = ResultData.error(err) },
            success = { response ->
                sourceResult.value = ResultData.success(response)
                emptyLastItem.value = response.sources?.isEmpty()
            }
        )

        try {


        } catch (e : Exception) {

            e.printStackTrace()
        }
        finally {

        }

//        launchDataLoad(flowData,{},{}) { data ->
//            emptyLastItem.value = data.sources?.isEmpty()
//            sourceLiveData.value = data.sources
//        }
    }
}