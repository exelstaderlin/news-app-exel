package com.exel.newsappexel.viewmodel

import androidx.lifecycle.MutableLiveData
import com.exel.newsappexel.api.VISIBLE_THRESHOLD
import com.exel.newsappexel.model.BaseArticleData
import com.exel.newsappexel.model.ResultData
import com.exel.newsappexel.repository.MainRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
class MainViewModel @Inject constructor(private var mainRepository: MainRepository) :
    BaseViewModel() {

    private var page: Int = 1

    val topHeadlineResult =
        MutableLiveData<ResultData<BaseArticleData>>() //Triggered LiveData when data is ready

    fun getHomePageData() {
        page = 1
        getHeadLineNews()
    }

    fun listScrolled(visibleItemCount: Int, firstVisibleItemPosition: Int, totalItemCount: Int) {
        if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
            && firstVisibleItemPosition >= 0
            && totalItemCount >= VISIBLE_THRESHOLD
        ) {
            if (!loadingRunning.value!!) { // avoid triggering multiple requests in the same time
                page++
                if (emptyLastItem.value == false) {
                    getHeadLineNews()
                    loadMoreLiveData.value = true
                } //not search when list is empty

            }
        }
    }

    private fun getHeadLineNews() {
        val flowData = mainRepository.getTopHeadLines(page)
        launchDataLoad(flowData,
            loading = { topHeadlineResult.value = ResultData.loading() },
            error = { err -> topHeadlineResult.value = ResultData.error(err) },
            success = { response ->
                topHeadlineResult.value = ResultData.success(response)
                emptyLastItem.value = response.articles?.isEmpty()
            }
        )
    }

}