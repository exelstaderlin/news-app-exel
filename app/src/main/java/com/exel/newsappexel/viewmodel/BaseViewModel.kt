package com.exel.newsappexel.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.exel.newsappexel.model.Status
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
abstract class BaseViewModel : ViewModel() {

    val loadingRunning = MutableLiveData<Boolean>()

    val emptyLastItem = MutableLiveData<Boolean>()

    val error = MutableLiveData<Throwable>()

    val loadMoreLiveData = MutableLiveData<Boolean>() //Triggered LiveData when data is ready

    fun <T> launchDataLoad(
        flowEmitData: Flow<T>,
        loading: suspend () -> Unit,
        error: suspend (throwable : Throwable) -> Unit,
        success: suspend (data: T) -> Unit
    ) {
        //viewModelScope Function (Default Dispatcher Main)
        viewModelScope.launch {
            flowEmitData
                .onStart {
                    loading()
                    loadingRunning.value = true
                }
                .catch { throwable ->
                    error(throwable)
                    loadingRunning.value = false
//                    error.value = throwable
                    Log.d("throwable : ", throwable.toString())
                }
                .collect { data -> //response yang sudah ready akan di collect dan ready untuk di kirim ke UI Thread
                    loadingRunning.value = false
                    success(data)
                }
        }
    }

}