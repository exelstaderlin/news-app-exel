package com.exel.newsappexel.viewmodel

import androidx.lifecycle.MutableLiveData
import com.exel.newsappexel.api.VISIBLE_THRESHOLD
import com.exel.newsappexel.model.ArtikelData
import com.exel.newsappexel.model.BaseArticleData
import com.exel.newsappexel.model.ResultData
import com.exel.newsappexel.repository.SearchRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
class SearchViewModel @Inject constructor(private val searchRepository: SearchRepository) :
    BaseViewModel() {
    private var page: Int = 1
    private var lastQuery: String? = null

    val searchResult = MutableLiveData<ResultData<BaseArticleData>>() //Triggered LiveData when data is ready

    fun searchUserData(username: String) { //button search event
        page = 1
        when {
            username.isNotEmpty() -> {
                lastQuery = username
//                searchBarLoading.value = true
                emptyLastItem.value = false
                getUserData()
            }
            else -> emptyLastItem.value = true
        }

    }

    fun listScrolled(visibleItemCount: Int, firstVisibleItemPosition: Int, totalItemCount: Int) {
        if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
            && firstVisibleItemPosition >= 0
            && totalItemCount >= VISIBLE_THRESHOLD
        ) {
            if (lastQuery != null && !loadingRunning.value!!) { // avoid triggering multiple requests in the same time
                page++
                if (emptyLastItem.value == false) //not search when list is empty
                    getUserData()
            }
        }
    }

    private fun getUserData() {
        val flowData = searchRepository.getSearchNewsData(lastQuery!!, page)
        launchDataLoad(flowData,
            loading = { searchResult.value = ResultData.loading() },
            error = { err -> searchResult.value = ResultData.error(err) },
            success = { response ->
                searchResult.value = ResultData.success(response)
                emptyLastItem.value = response.articles?.isEmpty()
            }
        )
    }
}