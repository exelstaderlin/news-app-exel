package com.exel.newsappexel.di.module

import android.content.Context
import android.util.Log
import android.view.View
import android.webkit.WebSettings
import com.exel.newsappexel.BaseApplication
import com.exel.newsappexel.api.ApiInterface
import com.exel.newsappexel.api.TOKEN
import com.exel.newsappexel.api.URL
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.ExperimentalCoroutinesApi
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@ExperimentalCoroutinesApi
@Module(
    includes = [ViewModelModule::class,
        ActivityModule::class]
)
class AppModule {

    @Singleton
    @Provides
    fun provideContext(application: BaseApplication): Context {
        return application.applicationContext
    }


    @Singleton
    @Provides
    fun provideHttpLogingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        return logging
    }

    @Singleton
    @Provides
    fun provideOkhttpClient(logging : HttpLoggingInterceptor): OkHttpClient {
        return OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .addInterceptor(logging)
            .addInterceptor { chain ->
                val original = chain.request()
                val request: Request
                request = original
                    .newBuilder()
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .also {
                        if (TOKEN.isNotEmpty()) {
                            it.header("Authorization", TOKEN)
                        }
                        Log.d("Authorization", TOKEN)
                    }
                    .method(original.method(), original.body())
                    .build()

                val response = chain.proceed(request)
                response
            }
            .build()
    }

    @Singleton
    @Provides
    fun provideService(client: OkHttpClient): ApiInterface {
        return Retrofit.Builder()
            .baseUrl(URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiInterface::class.java)
    }

}