package com.exel.newsappexel.di.module

import com.exel.newsappexel.ui.article.ArticleNewsActivity
import com.exel.newsappexel.ui.main.MainActivity
import com.exel.newsappexel.ui.main.SearchActivity
import com.exel.newsappexel.ui.main.WebViewActivity
import com.exel.newsappexel.ui.source.NewsSourcesActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@Suppress("unused")
@Module
internal abstract class ActivityModule {

    @ContributesAndroidInjector
    internal abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    internal abstract fun bindSearchActivity(): SearchActivity

    @ContributesAndroidInjector
    internal abstract fun bindListArticleNewsActivity(): ArticleNewsActivity

    @ContributesAndroidInjector
    internal abstract fun bindListNewsSourcesActivity(): NewsSourcesActivity

    @ContributesAndroidInjector
    internal abstract fun bindWebViewActivity(): WebViewActivity



}