package com.exel.newsappexel.ui.source

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.exel.newsappexel.databinding.ActivitySourcesListBinding
import com.exel.newsappexel.databinding.ToolbarHeaderBinding
import com.exel.newsappexel.model.BaseSourcesData
import com.exel.newsappexel.model.ResultData
import com.exel.newsappexel.model.Status
import com.exel.newsappexel.ui.main.BaseActivity
import com.exel.newsappexel.viewmodel.SourcesViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
class NewsSourcesActivity : BaseActivity() {

    @Inject
    lateinit var viewModelProvider: ViewModelProvider.Factory

    private var mCategory: String = ""
    private lateinit var mViewModel: SourcesViewModel
    private lateinit var mAdapter: SourcesAdapter
    private lateinit var binding: ActivitySourcesListBinding
    private lateinit var toolbarBinding: ToolbarHeaderBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySourcesListBinding.inflate(layoutInflater)
        toolbarBinding = binding.toolbarHeader
        setContentView(binding.root)
        initObject()
        initObserver()
        initListener()
        initAdapter()
        setToolbar(toolbarBinding.toolbarHeader, mCategory)
        getAllData()
    }

    private fun initObject() {
        mCategory = intent.getStringExtra("category") ?: ""
        mAdapter = SourcesAdapter()
        mViewModel = ViewModelProvider(this, viewModelProvider).get(SourcesViewModel::class.java)
    }

    private fun initListener() {
        binding.swipeRefreshLayout.setOnRefreshListener {
            getAllData()
        }
    }

    private fun initObserver() {
        mViewModel.sourceResult.observe(this) { result ->
            when (result.status) {
                Status.LOADING -> loadingEvent(binding.swipeRefreshLayout, true, null)
                Status.SUCCESS -> processData(result)
                Status.ERROR -> defaultErrorMessageHandling(result.error)
            }
        }
    }

    private fun initAdapter() {
        binding.recycleView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.recycleView.setHasFixedSize(true)
        binding.recycleView.adapter = mAdapter
    }

    private fun getAllData() {
        performNetwork {
            mViewModel.getAllData(mCategory)
        }
    }

    private fun processData(result: ResultData<BaseSourcesData>) {
        loadingEvent(binding.swipeRefreshLayout, false, null)
        val listData = result.data?.sources ?: emptyList()
        Log.d("Activity", "list: ${listData?.size}")
        mAdapter.submitList(listData) //Memakai DifUtils mengatasi masalah flickering pada view dan meningkatkan peforma
        emptyEvent(
            view = binding.emptyList,
            empty = listData.isEmpty()
        )
    }


}