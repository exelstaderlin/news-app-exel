package com.exel.newsappexel.ui.main

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.exel.newsappexel.R
import com.exel.newsappexel.databinding.CardMenuBinding
import com.exel.newsappexel.model.CategoryData
import com.exel.newsappexel.ui.source.NewsSourcesActivity
import kotlinx.android.synthetic.main.card_menu.view.*


class MenuAdapter(var listCategoryData: List<CategoryData>) :
    RecyclerView.Adapter<MenuAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            CardMenuBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listCategoryData[position])
        holder.itemBinding.container.setOnClickListener {
            val intent = Intent(holder.itemBinding.context, NewsSourcesActivity::class.java)
            intent.putExtra("category", listCategoryData[position].title)
            holder.itemBinding.context.startActivity(intent)
        }

    }

    inner class ViewHolder(private val binding: CardMenuBinding) : RecyclerView.ViewHolder(binding.root) {
        val itemBinding = binding.root
        fun bind(sourceData: CategoryData) {
            binding.apply {
                data = sourceData
                executePendingBindings()
            }
        }
    }


    override fun getItemCount(): Int {
        return listCategoryData.size
    }

}
