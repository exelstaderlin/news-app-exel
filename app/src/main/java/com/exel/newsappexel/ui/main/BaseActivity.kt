package com.exel.newsappexel.ui.main

import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.exel.newsappexel.R
import com.exel.newsappexel.util.checkInternetConnection
import com.exel.newsappexel.util.showAlertDialog
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.DaggerAppCompatActivity
import retrofit2.HttpException

/**
 * Created by Exel staderlin on 11/3/2018.
 */

abstract class BaseActivity : DaggerAppCompatActivity() {

    fun <T : Throwable> defaultErrorMessageHandling(liveData: MutableLiveData<T>, observer: Observer<T>) {
        liveData.observe(this, observer)
        checkHttpException(liveData)
    }

    fun <T : Throwable> defaultErrorMessageHandling(liveData: MutableLiveData<T>) {
        checkHttpException(liveData)
    }

    fun defaultErrorMessageHandling(err: Throwable?) {
        if (err is HttpException) {
            httpExceptionHandling(err)
        } else {
            Snackbar.make(findViewById(android.R.id.content), err.toString(), Snackbar.LENGTH_LONG).show()
        }
    }

    fun emptyEvent(view: View, empty : Boolean, prevData : Boolean) {
        view.visibility = if (empty && prevData) View.VISIBLE else View.GONE
    }

    fun emptyEvent(view: View, empty : Boolean) {
        view.visibility = if (empty) View.VISIBLE else View.GONE
    }


    fun loadingEvent(view: View, visibility: Boolean, block: (() -> Unit)?) {
        if(view is SwipeRefreshLayout)
            view.isRefreshing = visibility
        else
            view.visibility = if(visibility) View.VISIBLE else View.GONE

        if (block != null) block()
    }


    private fun <T : Throwable> checkHttpException(liveData: MutableLiveData<T>) {
        liveData.observe(this, Observer<T> {
            it?.printStackTrace()
            if (it is HttpException) {
                httpExceptionHandling(it)
            } else {
                Snackbar.make(findViewById(android.R.id.content), it.toString(), Snackbar.LENGTH_LONG).show()
            }
        })
    }

    fun performNetwork(onValid: () -> Unit) {
        when(checkInternetConnection(this)) {
            true -> onValid()
            else -> showAlertDialog(this, "Please turn on your network"){/* do nothing */}
        }
    }

    fun setToolbar(toolbar: Toolbar, title: String) {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.title = title
        toolbar.setNavigationOnClickListener { onBackPressed() }
    }


    private fun httpExceptionHandling(e: HttpException) {
        when (e.code()) {
            400 -> showAlertDialog(this, getString(R.string.bad_request)){ finish() }
            401 -> showAlertDialog(this, getString(R.string.session_expired)) {/* do nothing */}
            403 -> showAlertDialog(this, getString(R.string.error_access_right)){/* do nothing */}
            404 -> showAlertDialog(this, getString(R.string.error_page_doesnt_exist)){/* do nothing */}
            408 -> showAlertDialog(this, getString(R.string.error_rto)){/* do nothing */}
            410 -> showAlertDialog(this, getString(R.string.page_no_longer_available)) {/* do nothing */}
            422 -> showAlertDialog(this, getString(R.string.error_data_invalid)) {/* do nothing */}
            500 -> showAlertDialog(this, getString(R.string.error_internal_server)){/* do nothing */}
            else -> showAlertDialog(this, getString(R.string.error_submit_problem)){/* do nothing */}

        }
    }

}