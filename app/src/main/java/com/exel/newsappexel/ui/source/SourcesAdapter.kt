package com.exel.newsappexel.ui.source

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.exel.newsappexel.R
import com.exel.newsappexel.databinding.CardSourcesBinding
import com.exel.newsappexel.model.SourceData
import com.exel.newsappexel.ui.article.ArticleNewsActivity
import com.exel.newsappexel.ui.article.VIEW_TYPE_NORMAL
import kotlinx.coroutines.ExperimentalCoroutinesApi

class SourcesAdapter:
    ListAdapter<SourceData, RecyclerView.ViewHolder>(DATA_COMPARATOR) { //DifUtils -> Compare old data with new data

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            CardSourcesBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    @ExperimentalCoroutinesApi
    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == VIEW_TYPE_NORMAL) {
            holder as ViewHolder
            val listData = getItem(position)
            holder.bind(listData)
            holder.itemBinding.setOnClickListener {
                val intent = Intent(holder.itemView.context, ArticleNewsActivity::class.java)
                intent.putExtra("source", listData.id)
                holder.itemBinding.context.startActivity(intent)
            }
        }

    }

    inner class ViewHolder(private val binding: CardSourcesBinding) : RecyclerView.ViewHolder(binding.root) {
        val itemBinding = binding.root
        fun bind(sourceData: SourceData) {
            binding.apply {
                data = sourceData
                executePendingBindings()
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return VIEW_TYPE_NORMAL
    }

    companion object {
        //DiffUtil uses Eugene W. Myers's difference algorithm to calculate the minimal number of updates to convert one list into another.
        private val DATA_COMPARATOR = object : DiffUtil.ItemCallback<SourceData>() {
            override fun areItemsTheSame(oldItem: SourceData, newItem: SourceData): Boolean =
                oldItem.name == newItem.name

            override fun areContentsTheSame(oldItem: SourceData, newItem: SourceData): Boolean =
                oldItem == newItem
        }
    }
}
