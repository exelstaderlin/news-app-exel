package com.exel.newsappexel.ui.main

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.exel.newsappexel.R
import com.exel.newsappexel.api.VISIBLE
import com.exel.newsappexel.databinding.ActivityMainBinding
import com.exel.newsappexel.model.*
import com.exel.newsappexel.ui.article.ArticleAdapter
import com.exel.newsappexel.viewmodel.MainViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject


@ExperimentalCoroutinesApi
class MainActivity : BaseActivity() {

    @Inject
    lateinit var viewModelProvider: ViewModelProvider.Factory

    private lateinit var mViewModel: MainViewModel
    private lateinit var mAdapter: ArticleAdapter
    private lateinit var mListData: ArrayList<ArtikelData>
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initObject()
        initListener()
        initObserver()
        initCategoriesAdapter()
        initArtikelAdapter()
        getHomepageData()
    }

    private fun initObject() {
        mListData = ArrayList()
        mAdapter = ArticleAdapter(VISIBLE)
        mViewModel = ViewModelProvider(this, viewModelProvider).get(MainViewModel::class.java)
    }

    private fun initListener() {

        (binding.header).searchBtn.setOnClickListener {
            val intent = Intent(this, SearchActivity::class.java)
            startActivity(intent)
        }

        binding.nestedScrollView.setOnScrollChangeListener { v: NestedScrollView, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int ->
            if (v.getChildAt(v.childCount - 1) != null) {
                if ((scrollY >= (v.getChildAt(v.childCount - 1).measuredHeight - v.measuredHeight)) &&
                    scrollY > oldScrollY
                ) {
                    Log.i("Scroll", "BOTTOM SCROLL");
                    val layoutManager = binding.recycleViewArtikel.layoutManager as LinearLayoutManager
                    val totalItemCount = layoutManager.itemCount
                    val visibleItemCount = layoutManager.childCount
                    val firstVisibleItem = layoutManager.findFirstVisibleItemPosition()
                    mViewModel.listScrolled(visibleItemCount, firstVisibleItem, totalItemCount)
                }
            }
        }

        binding.swipeRefreshLayout.setOnRefreshListener {
            mListData = ArrayList()
            getHomepageData()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun initObserver() {
        mViewModel.topHeadlineResult.observe(this) { result ->
            when (result.status) {
                Status.LOADING -> loadingEvent(binding.swipeRefreshLayout, true) { mAdapter.loading = true }
                Status.SUCCESS -> processData(result)
                Status.ERROR -> defaultErrorMessageHandling(result.error)
            }
        }

        mViewModel.loadMoreLiveData.observe(this) {
            loadingEvent(binding.swipeRefreshLayout, false) { mAdapter.loading = true }
            addTemporaryEmptyData() //add temp data untuk loading bottom
        }
    }

    private fun initCategoriesAdapter() {
        val categoryListData = getCategoriesData()
        binding.recycleViewMenu.layoutManager = GridLayoutManager(this, 4)
        binding.recycleViewMenu.isNestedScrollingEnabled = false
        binding.recycleViewMenu.setHasFixedSize(true)
        binding.recycleViewMenu.adapter =
            MenuAdapter(categoryListData)
    }

    private fun initArtikelAdapter() {
        binding.recycleViewArtikel.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.recycleViewArtikel.isNestedScrollingEnabled = false
        binding.recycleViewArtikel.setHasFixedSize(true)
        binding.recycleViewArtikel.adapter = mAdapter
    }

    private fun getCategoriesData(): List<CategoryData> {
        val listCategoryData = ArrayList<CategoryData>()
        val categoryList = arrayListOf(
            "Business",
            "General",
            "Health",
            "Science",
            "Sports",
            "Technology",
            "Entertainment"
        )
        val categoryListIcon = arrayListOf(
            R.drawable.ic_business,
            R.drawable.ic_public,
            R.drawable.ic_health,
            R.drawable.ic_school,
            R.drawable.ic_bike,
            R.drawable.ic_lamp,
            R.drawable.ic_movie
        )
        for (i in 0 until categoryList.size) {
            val categoryData = CategoryData()
            categoryData.title = categoryList[i]
            categoryData.icon = categoryListIcon[i]
            listCategoryData.add(categoryData)
        }
        return listCategoryData
    }

    private fun getHomepageData() {
        performNetwork {
            mViewModel.getHomePageData()
        }
    }

    private fun processData(result: ResultData<BaseArticleData>) {
        loadingEvent(binding.swipeRefreshLayout, false) { mAdapter.loading = false }
        removeEmptyData() //remove temp data untuk loading bottom
        val articleList = result.data?.articles ?: emptyList()
        Log.d("Activity", "list: ${articleList.size}")
        articleList.forEach { item -> mListData.add(item.copy()) }
        mAdapter.submitList(mListData) //Memakai DifUtils mengatasi masalah flickering pada view dan meningkatkan peforma
        emptyEvent(
            view = binding.emptyList,
            empty = articleList.isEmpty(),
            prevData = mListData.isEmpty()
        )
    }

    private fun addTemporaryEmptyData() {
        mListData.add(ArtikelData())
        mAdapter.notifyItemInserted(mListData.size - 1);
    }

    private fun removeEmptyData() {
        if (mListData.size != 0) {
            mListData.removeAt(mListData.size - 1)
            val scrollPosition: Int = mListData.size
            mAdapter.notifyItemRemoved(scrollPosition)
        }
    }
}
