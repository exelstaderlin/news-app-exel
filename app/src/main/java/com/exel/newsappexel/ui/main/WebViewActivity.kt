package com.exel.newsappexel.ui.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import com.exel.newsappexel.R
import com.exel.newsappexel.databinding.ActivityWebViewBinding
import com.exel.newsappexel.databinding.ToolbarHeaderBinding
import kotlinx.android.synthetic.main.activity_web_view.*
import kotlinx.android.synthetic.main.toolbar_header.*

class WebViewActivity : BaseActivity() {

    private var mUrl: String? = null
    private var title: String? = null
    private lateinit var binding: ActivityWebViewBinding
    private lateinit var toolbarBinding: ToolbarHeaderBinding

    private var webViewClients = object : WebViewClient() {
        override fun onPageFinished(view: WebView, url: String) {
            setProgressBarIndeterminateVisibility(false)
            binding.progressBar.visibility = View.GONE
            super.onPageFinished(view, url)
        }

        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            view.loadUrl(url)
            return false
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWebViewBinding.inflate(layoutInflater)
        toolbarBinding = binding.toolbarHeader
        setContentView(binding.root)
        initObject()
        setToolbar(toolbarBinding.toolbarHeader, title ?:"")
        initWebView()
    }

    private fun initObject() {
        mUrl = intent.getStringExtra("url")
        title = intent.getStringExtra("title")
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView() {
        binding.webView.apply {
            settings?.apply {
                domStorageEnabled = true
                loadsImagesAutomatically = true
                javaScriptEnabled = true
                databaseEnabled = true
                allowFileAccess = true
                allowContentAccess = true
            }
            scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
            webViewClient = webViewClients
            Log.d("url", mUrl.toString())
            loadUrl(mUrl)
        }
    }
}