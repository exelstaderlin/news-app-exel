package com.exel.newsappexel.ui.article

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ObservableBoolean
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.exel.newsappexel.R
import com.exel.newsappexel.databinding.CardArtikelBinding
import com.exel.newsappexel.model.ArtikelData
import com.exel.newsappexel.ui.main.WebViewActivity
import kotlinx.android.synthetic.main.card_artikel.view.*
import kotlinx.android.synthetic.main.custom_article_view.view.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.text.category


const val VIEW_TYPE_NORMAL = 0
const val VIEW_TYPE_LOADING = 1

@ExperimentalCoroutinesApi
class ArticleAdapter(var mVisibility : Boolean) :
    ListAdapter<ArtikelData, RecyclerView.ViewHolder>(DATA_COMPARATOR) { //DifUtils -> Compare old data with new data

    var loading: Boolean = true
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        return if (viewType == VIEW_TYPE_NORMAL) return ViewHolder(
            CardArtikelBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
        else LoadingViewHolder(
            layoutInflater.inflate(
                R.layout.card_loading,
                parent,
                false
            )
        ) //Loading layout
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == VIEW_TYPE_NORMAL) {
            holder as ViewHolder
            val listData = getItem(position)
            holder.bind(listData)
            holder.itemBinding.setOnClickListener {
                val intent = Intent(holder.itemView.context, WebViewActivity::class.java)
                intent.putExtra("url", listData.url)
                intent.putExtra("title", listData.title)
                holder.itemView.context.startActivity(intent)
            }
            holder.itemBinding.category.setOnClickListener {
                val intent = Intent(holder.itemView.context, ArticleNewsActivity::class.java)
                intent.putExtra("source", listData.source?.id)
                holder.itemView.context.startActivity(intent)
            }
        }

    }

    inner class ViewHolder(private val binding: CardArtikelBinding) : RecyclerView.ViewHolder(binding.root) {
        val itemBinding = binding.root
        fun bind(artikelData: ArtikelData) {
            binding.customView.mBinding?.apply {
                data = artikelData
                executePendingBindings()
            }
            binding.customView.categoryVisibility = mVisibility
        }
    }

    inner class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun getItemViewType(position: Int): Int {
        return if (loading && position == itemCount - 1) {
            VIEW_TYPE_LOADING
        } else {
            VIEW_TYPE_NORMAL
        }
    }

    companion object {
        //DiffUtil uses Eugene W. Myers's difference algorithm to calculate the minimal number of updates to convert one list into another.
        private val DATA_COMPARATOR = object : DiffUtil.ItemCallback<ArtikelData>() {
            override fun areItemsTheSame(oldItem: ArtikelData, newItem: ArtikelData): Boolean =
                oldItem.title == newItem.title

            override fun areContentsTheSame(oldItem: ArtikelData, newItem: ArtikelData): Boolean =
                oldItem == newItem
        }
    }
}
