package com.exel.newsappexel.ui.main

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.exel.newsappexel.R
import com.exel.newsappexel.api.VISIBLE
import com.exel.newsappexel.databinding.ActivitySearchBinding
import com.exel.newsappexel.databinding.SearchLayoutBinding
import com.exel.newsappexel.databinding.ToolbarHeaderBinding
import com.exel.newsappexel.model.ArtikelData
import com.exel.newsappexel.model.BaseArticleData
import com.exel.newsappexel.model.ResultData
import com.exel.newsappexel.model.Status
import com.exel.newsappexel.ui.article.ArticleAdapter
import com.exel.newsappexel.viewmodel.SearchViewModel
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.search_layout.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
class SearchActivity : BaseActivity() {

    @Inject
    lateinit var viewModelProvider: ViewModelProvider.Factory

    private lateinit var mViewModel: SearchViewModel
    private lateinit var mAdapter: ArticleAdapter
    private lateinit var mListData: ArrayList<ArtikelData>
    private lateinit var binding: ActivitySearchBinding
    private lateinit var searchLayoutBinding: SearchLayoutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySearchBinding.inflate(layoutInflater)
        searchLayoutBinding = binding.searchHeader
        setContentView(binding.root)
        initObject()
        initObserver()
        initAdapter()
        initListener()
    }

    private fun initObject() {
        mListData = ArrayList()
        mAdapter = ArticleAdapter(VISIBLE)
        mViewModel = ViewModelProvider(this, viewModelProvider).get(SearchViewModel::class.java)
    }

    private fun initObserver() {
        mViewModel.searchResult.observe(this) { result ->
            when (result.status) {
                Status.LOADING -> loadingEvent(searchLayoutBinding.searchProgressBar, true) { mAdapter.loading = true }
                Status.SUCCESS -> processData(result)
                Status.ERROR -> defaultErrorMessageHandling(result.error)
            }
        }
    }

    private fun initAdapter() {
        binding.recycleListSearch.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.recycleListSearch.setHasFixedSize(true)
        binding.recycleListSearch.adapter = mAdapter
    }

    private fun initListener() {
        searchLayoutBinding.backBtn.setOnClickListener {
            onBackPressed()
        }

        searchLayoutBinding.searchEditText.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                hideKeyboard()
                searchUserData()
            }
            true
        }

        binding.recycleListSearch.addOnScrollListener(object :
            RecyclerView.OnScrollListener() { //scroll listener
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val layoutManager = binding.recycleListSearch.layoutManager as LinearLayoutManager
                val totalItemCount = layoutManager.itemCount
                val visibleItemCount = layoutManager.childCount
                val firstVisibleItem = layoutManager.findFirstVisibleItemPosition()

                mViewModel.listScrolled(visibleItemCount, firstVisibleItem, totalItemCount)
            }
        })
    }

    private fun hideKeyboard() {
        searchLayoutBinding.searchEditText.clearFocus()
        val inputManager: InputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(searchLayoutBinding.searchEditText.windowToken, 0) //hide keyboard
    }

    private fun processData(result: ResultData<BaseArticleData>) {
        loadingEvent(searchLayoutBinding.searchProgressBar, false) { mAdapter.loading = false }
        val articleList = result.data?.articles ?: emptyList()
        Log.d("Activity", "list: ${articleList.size}")
        articleList.forEach { item -> mListData.add(item.copy()) }
        mAdapter.submitList(mListData) //Memakai DifUtils mengatasi masalah flickering pada view dan meningkatkan peforma
        emptyEvent(
            view = binding.emptyList,
            empty = articleList.isEmpty(),
            prevData = mListData.isEmpty()
        )

    }


    private fun searchUserData() {
        performNetwork {
            searchLayoutBinding.searchEditText.text.trim().let { username ->
                binding.recycleListSearch.scrollToPosition(0)
                mViewModel.searchUserData(username.toString())
                mListData = ArrayList()
                mAdapter.submitList(null)
            }
        }
    }
}