package com.exel.newsappexel.ui.article

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.exel.newsappexel.api.GONE
import com.exel.newsappexel.databinding.ActivitySourcesListBinding
import com.exel.newsappexel.databinding.ToolbarHeaderBinding
import com.exel.newsappexel.model.ArtikelData
import com.exel.newsappexel.model.BaseArticleData
import com.exel.newsappexel.model.ResultData
import com.exel.newsappexel.model.Status
import com.exel.newsappexel.ui.main.BaseActivity
import com.exel.newsappexel.viewmodel.ArticleViewModel
import kotlinx.android.synthetic.main.activity_sources_list.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject


@ExperimentalCoroutinesApi
class ArticleNewsActivity : BaseActivity() {

    @Inject
    lateinit var viewModelProvider: ViewModelProvider.Factory

    private var mSource: String = ""
    private lateinit var mViewModel: ArticleViewModel
    private lateinit var mAdapter: ArticleAdapter
    private lateinit var mListData: ArrayList<ArtikelData>
    private lateinit var binding: ActivitySourcesListBinding
    private lateinit var toolbarBinding: ToolbarHeaderBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySourcesListBinding.inflate(layoutInflater)
        toolbarBinding = binding.toolbarHeader
        setContentView(binding.root)
        initObject()
        initObserver()
        initListener()
        initAdapter()
        setToolbar(toolbarBinding.toolbarHeader, mSource)
        getArticleData()
    }

    private fun initObject() {
        mSource = intent.getStringExtra("source") ?: ""
        mAdapter = ArticleAdapter(GONE)
        mListData = ArrayList()
        mViewModel = ViewModelProvider(this, viewModelProvider).get(ArticleViewModel::class.java)
    }

    private fun initListener() {
        binding.swipeRefreshLayout.setOnRefreshListener {
            getArticleData()
        }

        binding.recycleView.addOnScrollListener(object :
            RecyclerView.OnScrollListener() { //scroll listener
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val layoutManager = recycle_view.layoutManager as LinearLayoutManager
                val totalItemCount = layoutManager.itemCount
                val visibleItemCount = layoutManager.childCount
                val firstVisibleItem = layoutManager.findFirstVisibleItemPosition()

                mViewModel.listScrolled(visibleItemCount, firstVisibleItem, totalItemCount)
            }
        })

    }

    private fun initObserver() {
        mViewModel.articleResult.observe(this) { result ->
            when (result.status) {
                Status.LOADING -> loadingEvent(binding.swipeRefreshLayout, true) { mAdapter.loading = true }
                Status.SUCCESS -> processData(result)
                Status.ERROR -> defaultErrorMessageHandling(result.error)
            }
        }

        mViewModel.loadMoreLiveData.observe(this) {
            loadingEvent(binding.swipeRefreshLayout, false) { mAdapter.loading = true }
            addTemporaryEmptyData() //add temp data untuk loading bottom
        }

    }

    private fun initAdapter() {
        binding.recycleView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.recycleView.setHasFixedSize(true)
        binding.recycleView.adapter = mAdapter
    }

    private fun getArticleData() {
        performNetwork {
            mViewModel.getArticleData(mSource)
        }
    }

    private fun processData(result: ResultData<BaseArticleData>) {
        loadingEvent(binding.swipeRefreshLayout, false) { mAdapter.loading = false }
        removeEmptyData() //remove temp data untuk loading bottom
        val articleList = result.data?.articles ?: emptyList()
        Log.d("Activity", "list: ${articleList.size}")
        articleList.forEach { item -> mListData.add(item.copy()) }
        mAdapter.submitList(mListData) //Memakai DifUtils mengatasi masalah flickering pada view dan meningkatkan peforma
        emptyEvent(
            view = binding.emptyList,
            empty = articleList.isEmpty(),
            prevData = mListData.isEmpty()
        )
    }

    private fun addTemporaryEmptyData() {
        mListData.add(ArtikelData())
        mAdapter.notifyItemInserted(mListData.size - 1)
    }

    private fun removeEmptyData() {
        if (mListData.size != 0) {
            mListData.removeAt(mListData.size - 1)
            val scrollPosition: Int = mListData.size
            mAdapter.notifyItemRemoved(scrollPosition)
        }
    }


}