package com.exel.newsappexel.repository

import com.exel.newsappexel.api.ApiInterface
import com.exel.newsappexel.api.RestClient
import com.exel.newsappexel.model.BaseSourcesData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject


class SourcesRepository @Inject constructor(
    private val service : ApiInterface
){
    @ExperimentalCoroutinesApi
    fun getSources(
        category: String
    ): Flow<BaseSourcesData> {
        return flow {
            val request = service.getSources(category, "en")
            emit(request) //Emit request when response is coming and response ready to be collected
        }.flowOn(Dispatchers.IO) //Dispatcher IO run on background thread for network request
    }

}