package com.exel.newsappexel.repository


import com.exel.newsappexel.api.ApiInterface
import com.exel.newsappexel.api.NETWORK_PAGE_SIZE
import com.exel.newsappexel.model.BaseArticleData
import com.exel.newsappexel.model.ResultData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

@ExperimentalCoroutinesApi
class MainRepository @Inject constructor(
    private val service: ApiInterface
) {
    fun getTopHeadLines(
        page: Int
    ): Flow<BaseArticleData> {
        return flow {
            val request = service.getTopHeadLines("us", "general", page, NETWORK_PAGE_SIZE)
            emit(request) //Emit request when response is coming and response ready to be collected
        }.flowOn(Dispatchers.IO) //Dispatcher IO run on background thread for network request
    }
}