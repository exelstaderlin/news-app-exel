package com.exel.newsappexel.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


data class BaseArticleData(
    @SerializedName("status")
    @Expose
    val status: String? = null,

    @SerializedName("totalResults")
    @Expose
    val totalResults: Int? = null,

        @SerializedName("articles")
    @Expose
    val articles: List<ArtikelData>? = null
)