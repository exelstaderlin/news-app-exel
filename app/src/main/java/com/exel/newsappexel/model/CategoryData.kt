package com.exel.newsappexel.model

data class CategoryData (
    var title: String = "",
    var icon: Int = 0
)