package com.exel.newsappexel.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


data class BaseSourcesData(
    @SerializedName("status")
    @Expose
    val status: String? = null,

    @SerializedName("sources")
    @Expose
    val sources: List<SourceData>? = null
)