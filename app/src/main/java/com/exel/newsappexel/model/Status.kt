package com.exel.newsappexel.model

enum class Status {
    LOADING,
    SUCCESS,
    ERROR
}