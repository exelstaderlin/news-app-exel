package com.exel.newsappexel.model

data class ResultData<out T>(val status: Status, val data: T?, val error: Throwable?) {
    companion object {
        fun <T> success(data: T?): ResultData<T> {
            return ResultData(Status.SUCCESS, data, null)
        }

        fun <T> error(err : Throwable): ResultData<T> {
            return ResultData(Status.ERROR, null, err)
        }

        fun <T> loading(): ResultData<T> {
            return ResultData(Status.LOADING, null, null)
        }
    }
}
